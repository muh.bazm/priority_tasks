from flask import Blueprint, request, jsonify, make_response
from flask_restful import Api, Resource
from models import db, Task, TaskSchema
from sqlalchemy.exc import SQLAlchemyError
import status


api_bp = Blueprint('api', __name__)
task_schema = TaskSchema()

api = Api(api_bp)


class TaskResource(Resource):
    """
    This class handles HTTP requests for handling task-related requests.
    According to the requirements, only get and
    post methods are implemented and the rest are skipped.
    """

    def get(self):
        """
        Encapsulates the procedure related to GET requests.
        All previously inserted Tasks will be returned by sending
        a GET request to this resource.
        """
        task = Task.query.all()
        result = task_schema.dump(task, many=True)
        return result


    def post(self):
        """
        Encapsulates the procedure related to POST requests.
        A new task is inserted into the database by sending a POST
        request to this resource.
        """

        request_dict = request.get_json()
        if not request_dict:
            response = {'message': 'No input data provided'}
            return response, status.HTTP_400_BAD_REQUEST

        errors = task_schema.validate(request_dict)
        if errors:
            return errors, status.HTTP_400_BAD_REQUEST

        task = Task(id=request_dict['id'], priority=request_dict['priority'])
        task.add(task)

        return {'message': 'Task Created Successfully.'}




class TaskPerform(Resource):
    """
    This class encapsulates the procedure for serving all
    tasks in the database. A multi-queue round-robin approach is
    used for this services. Since there are three levels of
    priority, three lists are created to store related tasks.
    """
    def get(self):
        """
        A GET request starts task serving procedure.
        Tasks are stored in different queues corresponding to their
        priorities. Serving procedure includes multiple iterations.
        In each iteration, one task from each non-empty
        queue is poped and served. This strategy prevents
         an starvation situation for tasks.
        """

        tasks = Task.query.all()
        baskets = {1: [], 2: [], 3:[]}
        for task in tasks:
            priority = task.priority
            baskets[priority].append(task)

        while len(baskets[1]) != 0 or len(baskets[2]) != 0 or len(baskets[3]) != 0:

            if len(baskets[1]) != 0:
                task = baskets[1].pop()
                task.delete(task)

            if len(baskets[2]) != 0:
                task = baskets[2].pop()
                task.delete(task)

            if len(baskets[3]) != 0:
                task = baskets[3].pop()
                task.delete(task)

        return {'message': "All tasks are done"}



api.add_resource(TaskPerform, '/tasks_perform/')
api.add_resource(TaskResource, '/tasks/')

