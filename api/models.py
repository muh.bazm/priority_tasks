from marshmallow import Schema, fields, pre_load
from marshmallow import validate
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow


db = SQLAlchemy()
ma = Marshmallow()


class AddUpdateDelete():
    """
    This class encapsulates methods for CRUD operations. SQLAlchemy is used for this purpose.
    """
    def add(self, resource):
        """
        Insert an object into the database.
        """
        db.session.add(resource)
        return db.session.commit()

    def update(self):
        """
        Updates an object in the database.
        """
        return db.session.commit()

    def delete(self, resource):
        """
        Deletes an object from the database.
        """
        db.session.delete(resource)
        return db.session.commit()


class Task(db.Model, AddUpdateDelete):
    """
    Represents a model for Task objects.
    Task objects will be created corresponding to this class.
    A task has an integer ID and an integer priority.
    The priority has only three levels.
    """
    id = db.Column(db.Integer, primary_key=True)
    priority = db.Column(db.Integer, nullable=False)

    def __init__(self, id, priority):
        self.id = id
        self.priority = priority

class TaskSchema(ma.Schema):
    """
    This class encapsulates the schema
    according to which Task objects will be evaluated.
    """
    id = fields.Integer(required=True)
    priority = fields.Integer(required=True, validate=validate.Range(min=1, max=3))
